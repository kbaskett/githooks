"""Contains shared utilities for the git hooks."""

import contextlib
import functools
import glob
import os
import subprocess


@contextlib.contextmanager
def stash_files():
    """Context manager for stashing and restoring changes that haven't been
    committed.
    """
    print_header('Stashing uncommitted changes')
    subprocess.call(('git', 'stash', '-u', '--keep-index'),
                    stdout=subprocess.PIPE)
    try:
        yield
    finally:
        print_header('Restoring stashed changes')
        subprocess.call(('git', 'stash', 'pop', '-q'),
                        stdout=subprocess.PIPE)


def print_header(header_text):
    """Formats and prints the given text as a header.

    The text is surrounded by '=' out to a width of 80 characters. It is also
    prepended with a blank line.

    Arguments:
        header_text (str): The header text to print
    """
    header_text = ' ' + header_text + ' '
    padding_size = round((80 - len(header_text)) / 2)
    print('\n' + ('=' * padding_size) + header_text + ('=' * padding_size))


@contextlib.contextmanager
def top_level_init():
    """Context manager for creating an __init__.py file at the top of the
    directory.

    This is used to allow tests to run from the tests directory and import
    files from the hooks directory.
    """
    delete_top_level_init = False
    top_level_init_path = os.path.join(os.getcwd(), '__init__.py')

    if not os.path.exists(top_level_init_path):
        open(top_level_init_path, 'a').close()
        delete_top_level_init = True
    try:
        yield
    finally:
        if delete_top_level_init:
            os.remove(top_level_init_path)


@contextlib.contextmanager
def virtualenv_active():
    """Context manager that activates a virtual environment for subprocess calls.

    The context manager works by monkey patching subprocess.Popen. The
    existing Popen command is replaced by a function that has the shell and
    env keyword arguments specified. The shell argument is set to True. The
    env argument is set to the current value of os.environ with the path to
    the virtual environment Scripts directory prepended.

    Raises:
        NotADirectoryError: An error is raised if no virtual environment is
            found in the current working directory
    """
    venvs = get_virtual_envs()
    if not venvs:
        raise NotADirectoryError(
            'No virtual environment exists in the current working directory:' +
            os.getcwd())

    # Set up the new environment
    local_env = os.environ.copy()
    for venv in venvs:
        scripts = os.path.join(venv, 'Scripts')
        local_env['PATH'] = scripts + ';' + local_env['PATH']

    print('Activating virtual environment')
    # Monkeypatch subprocess.Popen
    existing_popen = subprocess.Popen
    subprocess.Popen = functools.partial(existing_popen, shell=True,
                                         env=local_env)
    try:
        yield
    finally:
        # Restore subprocess.Popen to the original version
        subprocess.Popen = existing_popen
        print('Deactivating virtual environment')


def get_virtual_envs():
    """Return a list of the virtual environment paths in the current working
    directory.

    This function uses a glob search to find all the Scripts/python.exe files
    in the current working directory.

    Returns:
        list: A list of paths to virtual environment directories in the
            current working directory
    """
    venv_glob = os.path.join(os.getcwd(), '*', 'Scripts', 'python.exe')
    return [os.path.dirname(os.path.dirname(path)) for path in
            glob.iglob(venv_glob)]
