"""Test file for hooks/utils.py"""
import functools
import os
import subprocess

import pytest

from ..hooks import utils


@pytest.mark.parametrize('text', ['This is a test'])
def test_print_header(capsys, text):
    """Tests the print_header function."""
    utils.print_header(text)
    output, _ = capsys.readouterr()
    output = output.splitlines()[1]

    assert text in output
    assert output[0] == '='
    assert output[-2] == '='


def test_with_top_level_init(tmpdir):
    """Tests the with_top_level_init context manager."""
    with tmpdir.as_cwd():
        init_path = os.path.join(os.getcwd(), '__init__.py')

        assert not os.path.exists(init_path)
        with utils.top_level_init():
            assert os.path.isfile(init_path)
        assert not os.path.exists(init_path)


def test_get_virtual_envs():
    """Tests the get_virtual_envs function."""
    envs = utils.get_virtual_envs()
    assert len(envs) == 1
    env_path = os.path.join(os.getcwd(), 'venv')
    assert envs[0] == env_path


@pytest.mark.xfail
def test_virtualenv_active():
    """Tests the virtualenv_active context manager.

    This test is failing because pytest is run from the virtual environment
    python instance. So before, during and after are all equal.
    """
    python_call = functools.partial(
        subprocess.Popen,
        ('python', '-c', 'import sys;print(sys.executable)'),
        stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    proc = python_call()
    before, _ = proc.communicate()

    with utils.virtualenv_active():
        proc = python_call()
        during, _ = proc.communicate()

    assert before != during

    proc = python_call()
    after, _ = proc.communicate()

    assert during != after
    assert before == after


def test_virtualenv_active_error(tmpdir):
    """Tests the exception case for virtualenv_active."""
    with tmpdir.as_cwd():
        with pytest.raises(NotADirectoryError):
            with utils.virtualenv_active():
                # This line should not be reached
                assert True
