Post-commit Hook
================

The post-commit hook performs the following actions:

1. Build documentation using sphinx.
2. Display coverage report for unit tests and update html coverage report.

Usage
-----

::

    usage: post-commit.py [-h] [-d] [-c]

    Post-commit script that builds documentation using sphinx and runs a test
    coverage report. If none of the optional arguments are specified, then all are
    performed

    optional arguments:
      -h, --help      show this help message and exit
      -d, --doc       Build documentation for the repository using sphinx.
      -c, --coverage  Display coverage report for unit tests.

Calling from git
----------------

To call this from git on pre-commit, create .git/hooks/pre-commit with the following contents::

    #!/bin/sh
    python hooks/pre-commit.py

If using a virtual environment, use the following::

    #!/bin/sh
    ./venv/Scripts/python hooks/pre-commit.py
