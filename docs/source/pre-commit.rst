Pre-commit Hook
===============

The pre-commit hook performs the following checks:

1. Enforce pep8 compliance.
2. Check pep8 compliance. If this check is performed, and the check fails,
   the commit is blocked.
3. Run unit tests using pytest. If this is performed, and any of the tests 
   fail, the commit is blocked. Pytest is run using coverage. If the commit
   is successful, the coverage report is displayed by the post-commit hook.

Usage
-----

::

    usage: pre-commit.py [-h] [--enforce-pep8] [--check-pep8] [--pytest]

    Pre-commit git hook. If no command line options are specified, then all
    options are run.

    optional arguments:
      -h, --help      show this help message and exit
      --enforce-pep8  Runs pep8ify to enforce pep8 rules.
      --check-pep8    Checks for pep8 conformance. If the check fails, the commit
                      is blocked.
      --pytest        Runs unit tests using pytest.

Calling from git
----------------

To call this from git on pre-commit, create .git/hooks/pre-commit with the following contents::

    #!/bin/sh
    python hooks/pre-commit.py

If using a virtual environment, use the following::

    #!/bin/sh
    ./venv/Scripts/python hooks/pre-commit.py
