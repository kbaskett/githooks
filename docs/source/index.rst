.. GitHooks documentation master file, created by
   sphinx-quickstart on Sun Apr 24 21:53:27 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

GitHooks
========

GitHooks is a set of hooks for git to perform the continuous integration 
features I want included for all of my projects. This includes

* Ensuring unit tests are passing before allowing a commit.
* Building documentation after a commit.
* Updating test coverage reports.

Contents:

.. toctree::
   :maxdepth: 2

   pre-commit
   post-commit


Getting Started
===============

To begin using the hooks defined in GitHooks, copy the hooks subfolder
into your project folder. Then, add bash scripts to .git/hooks to call
those hooks. The hooks can be called using a virtual environment if 
appropriate.



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

